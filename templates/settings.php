<div class="wrap">
<h1>Kunye Ayarları</h1>

<p>
    Künye eklentisi ile firmanız hakkındaki bilgileri kullanıcının pano 
    sayfasında görünteleyebilirsiniz. Böylece ihtiyaç duyan kullanıcılar size
    hızlıca ulaşabilirler. Burada ayalarlar yapıldıktan sonra bu menü gizlenecektir.
</p>

<form action="<?php echo get_admin_url()."admin-post.php"; ?>" method="post">
    <input type="hidden" name="action" value="kunye_info_form" />
    <table class="form-table" role="presentation">
        <tbody>
            <tr>
                <th scope="row">
                    <label for="company">Firma Adı</label>
                </th>
                <td>
                    <input name="company" type="text" id="company" class="regular-text code">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="company_official">Yetkili Kişi</label>
                </th>
                <td>
                    <input name="company_official" type="text" id="company_official" class="regular-text ltr">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="phone">Telefon</label>
                </th>
                <td>
                    <input name="phone" type="text" id="phone" class="regular-text ltr">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="mail">E-posta</label>
                </th>
                <td>
                    <input name="mail" type="text" id="mail" class="regular-text ltr">
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="companyWeb">Firma Web Sayfası</label>
                </th>
                <td>
                    <input name="companyWeb" type="text" id="companyWeb" class="regular-text ltr">
                </td>
            </tr>
        </tbody>
    </table>

    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Değişiklikleri kaydet">
    </p>
</form>

</div>