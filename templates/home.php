<div class="kunye">
    <div class="kunyeDescription">
        Aşağıdaki bilgileri kullanarak bu sitenin yapımcısına ulaşabilir ve
        destek talebinde bulunabilirsiniz.
    </div>
    <?php if ($card_info) : ?>
        <div class="cardContainer">
            <div class="cardInfo">
                <div class="title"><strong>Firma</strong></div>
                <div class="content">: <a href="<?php echo $card_info->company_web ?>" target="_blank"><?php echo $card_info->company ?></a></div>

                <div class="title"><strong>Yetkili</strong></div>
                <div class="content">: <?php echo $card_info->company_official ?></div>

                <div class="title"><strong>Telefon</strong></div>
                <div class="content">: <?php echo $card_info->phone ?></div>

                <div class="title"><strong>E-posta</strong></div>
                <div class="content">: <?php echo $card_info->mail?></div>
            </div>
            <div>
                <img src="https://chart.googleapis.com/chart?chs=120x120&cht=qr&chl=<?php echo $card_info->company_web ?>&choe=UTF-8" alt="qr" />
            </div>
        </div>
        
    <?php else : ?>
        <div class="notification">
            <strong>Lütfen önce firma bilgilerini tanımlayın.</strong>
            <a href="<?php echo admin_url('admin.php?page=kunye') ?>">Firma Bilgilerini Gir</a>
        </div>
    <?php endif ?>
</div>