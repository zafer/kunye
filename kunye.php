<?php
/**
 * Plugin Name:     Kunye
 * Plugin URI:      https://solokod.com
 * Description:     Site yapımıcısının bilgilerini içeren bilgi alanı
 * Version:         1.0.0
 * Requires PHP:    7.4
 * Author:          Solokod
 * Author URI:      https://solokod.com
 * Text Domain:     kunye
 */

define("PLUGIN_DIR_PATH", plugin_dir_path(__FILE__));

require_once dirname(__FILE__) . "/vendor/autoload.php";

use Kunye\RegisterPlugin;
use Kunye\RegisterAssets;
use Kunye\DashboardWidget;

class Kunye {

    public function __construct() {
        new RegisterPlugin();
        new RegisterAssets();
        new DashboardWidget();
    }
}

new Kunye();