<?php

namespace Kunye;

class RegisterAssets {

    public function __construct() {
        add_action("admin_enqueue_scripts", [$this, "register_scripts"]);
    }

    public function register_scripts() {
        $base_url = plugin_dir_url(dirname(__FILE__, 1));

        wp_enqueue_style("kunye", $base_url . "assets/css/kunye.css");
        wp_enqueue_script("kunye", $base_url . "assets/js/kunye.js", array(), null, true);
    }
}