<?php

namespace Kunye;

class RegisterPlugin {

    public function __construct() {
        $file = plugin_dir_path(dirname(__FILE__, 1)) . "kunye.php";
        
        register_activation_hook($file, [$this, "activate"]);

        if (!file_exists(PLUGIN_DIR_PATH . "card-info.json")) {
            add_action("admin_menu", [$this, "kunye_add_options_page"]);
        }
    }

    public function kunye_add_options_page() {
        $page_title = "Kunye Ayarları";
        $menu_title = "Kunye Ayarları";
        $capability = "manage_options";
        $menu_slug = "kunye";
        $callback = [$this, "settings_page"];
        $position = 10;

        add_options_page($page_title, $menu_title, $capability, $menu_slug, 
            $callback, $position);
    }

    public function settings_page() {
        require_once PLUGIN_DIR_PATH . "templates/settings.php";
    }

    public function activate() {
        error_log("Plugin kayit ediliyor... (activation)");
    }
}