<?php

namespace Kunye;

class DashboardWidget {

    public function __construct() {
        add_action("wp_dashboard_setup", [$this, "dashboard_widget"]);
        add_action("admin_post_kunye_info_form", [$this, "info_form"]);
    }

    public function info_form() {
        $data = array(
            "company" => $_POST["company"],
            "company_web" => $_POST["company_web"],
            "company_official" => $_POST["company_official"],
            "phone" => $_POST["phone"],
            "mail" => $_POST["mail"],
        );

        $file = PLUGIN_DIR_PATH . "card-info.json";
        file_put_contents($file, json_encode($data));
        
        // redirect after insert alert
        //wp_redirect(admin_url('admin.php?page=kunye'));
        wp_redirect(admin_url('index.php'));
        die();
    }

    public function dashboard_widget() {
        wp_add_dashboard_widget(
            "dashboard_kunye",
            __("Site Künyesi", "kunye"),
            [$this, "dashboard_kunye_render"]
        );
    }

    public function dashboard_kunye_render() {
        $file = PLUGIN_DIR_PATH . "card-info.json";
        $card_info = wp_json_file_decode($file);

        require_once PLUGIN_DIR_PATH . "templates/home.php";
    }
}